package whether.info.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Path("/api")
public class WhetherService {

	@GET
	@Path("/report/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Object> getWhatherDetail(@PathParam("name") String name) {

		Map<String, Object> report = null;
		try {
			StringBuilder result = new StringBuilder();
			Map<String, Object> reportMap = getLatLong(name);

			String lat = (String) reportMap.get("latt");
			String lon = (String) reportMap.get("longt");
			System.out.println("lat lon = " + lat + " " + lon);
			String urlString = "https://fcc-weather-api.glitch.me/api/current?lat=" + lat + "&lon=" + lon;
			URL url = new URL(urlString);
			URLConnection conn = url.openConnection();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();
			System.out.println(result);
//			report = result.toString();

			Map<String, Object> breafMap = JsonToMap(result.toString());
			Map<String, Object> main = (Map<String, Object>) breafMap.get("main");
			System.out.println(main.toString());
			report = main;
		} catch (IOException e) {
			System.out.println(e.getMessage());
//			report = e.getMessage();
		}
		return report;
	}

	public Map<String, Object> getLatLong(String name) {
		Map<String, Object> reportMap = null;

		// String API_KEY = "1e190cd39a31f8c34bdf8fbcd508ba14";
		// String LOCATION ="Releigh,NC";
		// String urlString
		// ="http://api.openweathermap.org/data/2.5/weather?q="+LOCATION+"&appid="+API_KEY+"&units=imperial";
		try {
			StringBuilder result = new StringBuilder();
			System.out.println("name " + name);
			URL url = new URL("https://geocode.xyz/" + name + "?json=1");
			URLConnection conn = url.openConnection();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();
			reportMap = JsonToMap(result.toString());

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return reportMap;
	}

	public Map<String, Object> JsonToMap(String str) {
		return new Gson().fromJson(str, new TypeToken<HashMap<String, Object>>() {
		}.getType());
	}

}
