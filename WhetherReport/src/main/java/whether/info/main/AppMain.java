package whether.info.main;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.servlets.CrossOriginFilter;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import whether.info.config.AppConfig;
import whether.info.service.WhetherService;




public class AppMain extends Application<AppConfig> {

	public static void main(String[] args) throws Exception {
		new AppMain().run("server", "config.yml");

	}

	@Override
	public void run(AppConfig config, Environment env) throws Exception {

		// Enable CORS headers
		final FilterRegistration.Dynamic cors = env.servlets().addFilter("CORS", CrossOriginFilter.class);

		// Configure CORS parameters
		cors.setInitParameter("allowedOrigins", "*");
		cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
		cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

		// Add URL mapping
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
		env.jersey().register(new WhetherService());

	}

}
